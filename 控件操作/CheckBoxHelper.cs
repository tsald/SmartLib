﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DotNet.Utilities
{
    public static class CheckBoxHelper
    {
        /// <summary>
        /// 设置checkbox的状态
        /// </summary>
        /// <param name="result">1|0</param>
        /// <param name="cb">checkbox</param>
        public static void Set(string result,CheckBox cb)
        {
            //设置选中状态
            if (result=="1")
            {
                cb.Checked = true;
            }
            //设置未选中状态
            else
            {
                cb.Checked = false;
            }
        }
    }
}
