﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DotNet.Utilities
{
    public class CheckBoxListHelper
    {
        /// <summary>
        /// 数据绑定
        /// </summary>
        /// <param name="dic">数据源</param>
        /// <param name="ckl">需要绑定的checkboxlist</param>
        public static void DataBind(Dictionary<string, string> dic, CheckBoxList ckl)
        {
            if (dic.Count > 0)
            {
                foreach (var item in dic)
                {
                    ckl.Items.Add(new ListItem(item.Key, item.Value));
                }
            }
            else
            {
                ckl.Items.Add(new ListItem("没有数据", "0"));
            }
        }
        /// <summary>
        /// 获取勾选的checkboxlist的value
        /// </summary>
        /// <param name="ckl">数据源</param>
        /// <returns></returns>
        public static string GetCheckedValue(CheckBoxList ckl)
        {
            string result = "";
            if (ckl.Items.Count > 0)
            {
                foreach (ListItem item in ckl.Items)
                {
                    if (item.Selected == true)
                    {
                        result += item.Value + ",";
                    }
                }
            }
            if (ckl.Items.Count == 0)
            {
                return "";
            }
            result = DotNet.Utilities.StringHelper.DelLastComma(result);
            return result;

        }
        /// <summary>
        /// 选中checkboxlist
        /// </summary>
        /// <param name="strArry">value数组</param>
        /// <param name="cbl">源控件</param>
        public static void SelectCheckItems(string[] strArry,CheckBoxList cbl)
        {
            foreach (ListItem item in cbl.Items)
            {
                for (int i = 0; i < strArry.Length; i++)
                {
                    if (strArry[i]==item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
    }
}
