﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DotNet.Utilities
{
    public static class DropDownListHelper
    {
        /// <summary>
        /// 绑定dropdownlist数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">数据源</param>
        /// <param name="ddl">控件</param>
        /// <param name="text">选项文本</param>
        /// <param name="value">选项值</param>
        public static void DataBind<T>(List<T> list, DropDownList ddl, string text, string value)
        {
            if (list.Count > 0)
            {
                ddl.Items.Insert(0, new ListItem("---请选择---", "0"));
                ddl.DataSource = list;
                ddl.DataTextField = text;
                ddl.DataValueField = value;
                ddl.DataBind();

            }
            else
            {
                ddl.Items.Insert(0, new ListItem("---没有数据---", "-1"));

            }
        }
        /// <summary>
        /// 绑定dropdownlist数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">数据源</param>
        /// <param name="ddl">控件</param>
        /// <param name="text">选项文本</param>
        /// <param name="value">选项值</param>
        /// <param name="defult">默认选中项</param>
        public static void DataBind<T>(List<T> list, DropDownList ddl, string text, string value, string defult)
        {
            if (list.Count > 0)
            {
                ddl.Items.Insert(0, new ListItem("---请选择---", "0"));
                ddl.DataSource = list;
                ddl.DataTextField = text;
                ddl.DataValueField = value;
                ddl.DataBind();
                ddl.Items.FindByValue(defult).Selected = true;

            }
            else
            {
                ddl.Items.Insert(0, new ListItem("---没有数据---", "-1"));

            }
        }
        /// <summary>
        /// 绑定dropdownlist数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">数据源</param>
        /// <param name="ddl">控件</param>
        /// <param name="text">选项文本</param>
        /// <param name="value">选项值</param>
        /// <param name="defult">默认选中项</param>
        /// <param name="enable">是否可用</param>
        public static void DataBind<T>(List<T> list, DropDownList ddl, string text, string value, string defult, bool enable = true)
        {
            if (list.Count > 0)
            {
                ddl.Items.Insert(0, new ListItem("---请选择---", "0"));
                ddl.DataSource = list;
                ddl.DataTextField = text;
                ddl.DataValueField = value;
                ddl.DataBind();
                ddl.Items.FindByValue(defult).Selected = true;
                ddl.Enabled = enable;
            }
            else
            {
                ddl.Items.Insert(0, new ListItem("---没有数据---", "-1"));

            }
        }
        /// <summary>
        /// 默认选择项
        /// </summary>
        /// <param name="ddl">控件</param>
        /// <param name="value">默认选择的value</param>
        public static void DefultSelect(DropDownList ddl, string value)
        {
            ddl.Items.FindByValue(value).Selected = true;
        }
    }
}
