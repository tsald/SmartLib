﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DotNet.Utilities.异步线程
{
    //如果有一个步骤需要大量的计算怎使用多线程
    //如果多线程的话需要用lock
    public class ThreadManager
    {

        #region 用于有参数的委托传回返回值
        private static Object value;
        /// <summary>
        /// 用于有参数的委托传回返回值
        /// </summary>
        /// <param name="o"></param>
        static public void SetValue(Object o)
        {
            value = o;
        }
        #endregion

        #region 创建一个新线程（无参无返回值）
        /// <summary>
        /// 创建一个新线程（无参无返回值）
        /// </summary>
        /// <param name="pts">委托方法</param>
        /// <param name="sleep">挂起时间</param>
        public void CreateNewThread(ParameterizedThreadStart pts, int sleep = 0)
        {
            try
            {
                //创建线程
                Thread th = new Thread(pts);
                //开始线程     
                th.Start();
                //线程挂起
                Thread.Sleep(sleep);
            }
            catch (ThreadStateException ex)
            {

                throw ex;
            }
          

        }
        #endregion

        #region 创建一个新线程（有参数无返回值）
        /// <summary>
        /// 创建一个新线程（有参数无返回值）
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="o"></param>
        /// <param name="sleep"></param>
        public void CreateNewThread(ParameterizedThreadStart pts, Object o, int sleep = 0)
        {
            try
            {
                //创建线程
                Thread th = new Thread(ThreadManager.TestThreadObject);
                //开始线程
                th.Start(o);
                //挂起线程
                Thread.Sleep(sleep);
            }
            catch (ThreadStateException ex)
            {

                throw ex;
            }
      
        }
        #endregion

        #region 传入示例方法
        static public void TestThread()
        {
            //方法内容
            //SetValue(o)用于返回值
        }
        static public void TestThreadObject(object o)
        {
            //方法内容
        }
        #endregion


       
    }

}
