﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNet.Utilities.VTemplate
{
    public class VTemplateHelper
    {
        private string _templateName;

        public string TemplateName
        {
            get
            {
                return _templateName;
            }

            set
            {
                _templateName = value;
            }
        }
       

    }
}
